<!DOCTYPE html>	
<head>
<title></title>



<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" /> -->
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" /> -->



<?php require('head.php') ?>


<!-- 首頁輪播 -->
<link href="vendor/Owl/owl.carousel.css" rel="stylesheet" type="text/css" media="all" />
<!-- <link rel="stylesheet" href="vendor/Owl/owl.theme.default.css"> -->
<script src="vendor/Owl/owl.carousel.js"></script>
<script language="javascript">
    $(document).ready(function() {
        $('.owl-carousel').owlCarousel({
            animateOut: 'fadeOut',
            animateIn: 'flipInX',
            loop: true,
            margin: 0,
            stagePadding: 0,
            // smartSpeed: 200,
            autoplay: true, 
            autoplayTimeout: 6000,
            dots: false,
            nav:  false,
            responsive: {
                320: {
                    items: 1
                },
                1024: {
                    // items: 2
                    items: 1
                },
                1440: {
                    items: 1
                    // items: 2,
                    // margin: 50,
                },
                1860:{
                    items: 1
                }
            }
        });
    });

</script>



<body>
    
    <div>
        <a href="main.php" class="indLink"></a>
        <div id="indBk" class="indBk ptb-50 plr-50">
            <div class="indLeftBk">
                <section class="modLogoTitArea">
                    <img src="images/logo.png" alt="台中機場LOGO" class="modLogoTitArea-logo">
                    <h5 class="modLogoTitArea-enTit">
                        Taichung International
                        Airport  toilet satisfaction<br />
                        survey system<br />
                    </h5>
                </section>
                <div class="indLeftBk-pageTitArea">
                    <div class="indLeftBk-pageTitArea--ChangeTit">
                        <h3 class="js-titEn">Please press here to <br />Evalute toilets</h3>
                        <h3 class="js-titJp">トイレ満足度調査</h3>
                        <h3 class="js-titKr">화장실 만족도 조사</h3>
                    </div>
                    <h1 class="indLeftBk-pageTitArea--chTit">點擊螢幕畫面替廁所評分</h1>
                </div>
                <img src="images/decTriangle-01.svg" alt="" class="indLeftBk-pageTitArea--dec">
                <div class="indLeftBk-pageTitArea--decBt js-decBt">
                    <img src="images/triangle--maincolor.svg" alt="icon">
                </div>
            </div>
            <div class="indRightBk owl-carousel">
                <!-- 廣告圖建議尺寸 710*665 -->
                <img src="images/banner01.png" alt="banner" class="">
                <img src="images/banner02.png" alt="banner" class="">
            </div>
        </a>		
	  
    </div>
	

	
	
</body>
</html>
<script>

/* 
Native FullScreen JavaScript API
-------------
Assumes Mozilla naming conventions instead of W3C for now
*/

(function() {
	var 
		fullScreenApi = { 
			supportsFullScreen: false,
			isFullScreen: function() { return false; }, 
			requestFullScreen: function() {}, 
			cancelFullScreen: function() {},
			fullScreenEventName: '',
			prefix: ''
		},
		browserPrefixes = 'webkit moz o ms khtml'.split(' ');
	
	// check for native support
	if (typeof document.cancelFullScreen != 'undefined') {
		fullScreenApi.supportsFullScreen = true;
	} else {	 
		// check for fullscreen support by vendor prefix
		for (var i = 0, il = browserPrefixes.length; i < il; i++ ) {
			fullScreenApi.prefix = browserPrefixes[i];
			
			if (typeof document[fullScreenApi.prefix + 'CancelFullScreen' ] != 'undefined' ) {
				fullScreenApi.supportsFullScreen = true;
				
				break;
			}
		}
	}
	
	// update methods to do something useful
	if (fullScreenApi.supportsFullScreen) {
		fullScreenApi.fullScreenEventName = fullScreenApi.prefix + 'fullscreenchange';
		
		fullScreenApi.isFullScreen = function() {
			switch (this.prefix) {	
				case '':
					return document.fullScreen;
				case 'webkit':
					return document.webkitIsFullScreen;
				default:
					return document[this.prefix + 'FullScreen'];
			}
		}
		fullScreenApi.requestFullScreen = function(el) {
			return (this.prefix === '') ? el.requestFullScreen() : el[this.prefix + 'RequestFullScreen']();
		}
		fullScreenApi.cancelFullScreen = function(el) {
			return (this.prefix === '') ? document.cancelFullScreen() : document[this.prefix + 'CancelFullScreen']();
		}		
	}

	// jQuery plugin
	if (typeof jQuery != 'undefined') {
		jQuery.fn.requestFullScreen = function() {
	
			return this.each(function() {
				var el = jQuery(this);
				if (fullScreenApi.supportsFullScreen) {
					fullScreenApi.requestFullScreen(el);
				}
			});
		};
	}

	// export api
	window.fullScreenApi = fullScreenApi;	
})();

</script>

<script>

// do something interesting with fullscreen support
var fsButton = document.getElementById('fsbutton');
var	fsElement = document.getElementById('specialstuff');

if (window.fullScreenApi.supportsFullScreen) {
	// fsStatus.innerHTML = 'YES: Your browser supports FullScreen';
	// fsStatus.className = 'fullScreenSupported';
	
	// handle button click
	// fsButton.addEventListener('click', function() {
	// 	window.fullScreenApi.requestFullScreen(fsElement);
	// }, true);

	
	if ($.cookie("playbgmusic") == "true") {
		// $("#sysaudio")[0].play();
		// $("#AudioPlayBtn").prop("src", "images/onRadio-icon.svg");
		window.fullScreenApi.requestFullScreen(fsElement);
	} else if ($.cookie("playbgmusic") == "false") {
		// $("#sysaudio")[0].pause();
		// $("#AudioPlayBtn").prop("src", "images/muteRadio-icon.svg");
	}
	console.log($.cookie("playbgmusic"));  
	
	// fsElement.addEventListener(fullScreenApi.fullScreenEventName, function() {
	// 	if (fullScreenApi.isFullScreen()) {
	// 		fsStatus.innerHTML = 'Whoa, you went fullscreen';
	// 	} else {
	// 		fsStatus.innerHTML = 'Back to normal';
	// 	}
	// }, true);
	
} else {
	// fsStatus.innerHTML = 'SORRY: Your browser does not support FullScreen';
};


</script>
