<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" /> -->
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" /> -->

<!-- 自動跳轉頁面 -->
<meta http-equiv="refresh" content="21;url= index.php ">

<?php require('head.php') ?>



<script language="javascript">
    
</script>


<body class="">
	<div class="pagMainBk">
		<div class="pagMainLeftBk ptb-50 pl-50 pr-50">
			<section class="modLogoTitArea">
				<img src="images/logo.png" alt="台中機場LOGO" class="modLogoTitArea-logo">
				<h5 class="modLogoTitArea-enTit">
					Taichung International
					Airport  toilet satisfaction<br />
					survey system<br />
				</h5>
			</section>
			<div class="pagMainLeftBk-pageTitArea">
				<div class="pagMainLeftBk-pageTitArea--ChangeTit">
                    <h3 class="js-titEn">Please press here to <br /> Evalute toilets</h3>
                    <h3 class="js-titJp">トイレ満足度調査</h3>
                    <h3 class="js-titKr">화장실 만족도 조사</h3>
                </div>
				<h1 class="pagMainLeftBk-pageTitArea--chTit">點擊螢幕畫面替廁所評分</h1>
			</div>
			<img src="images/decTriangle-02.svg" alt="" class="pagMainLeftBk-pageTitArea--dec">
            <div class="pagMainLeftBk-pageTitArea--decBt js-decBt">
                <img src="images/triangle--maincolor.svg" alt="icon">
            </div>
		</div>
        <div class="pagMainRightBk">
            <a href="result-good.php" class="pagMainRightBk-list">
                <h2 class="pagMainRightBk-list--chtit">非常滿意</h2>
                <div class="pagMainRightBk-list--titStarArea">
                    <div class="pagMainRightBk-list--changeTitArea">
                        <h4 class="js-titEn">Excellent</h4>
                        <h4 class="js-titJp">非常に満足</h4>
                        <h4 class="js-titKr">매우 만족</h4>
                    </div>
                    <div class="pagMainRightBk-list--starArea">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                    </div>
                </div>
            </a>
            <a href="result-good.php" class="pagMainRightBk-list">
                <h2 class="pagMainRightBk-list--chtit">滿意</h2>
                <div class="pagMainRightBk-list--titStarArea">
                    <div class="pagMainRightBk-list--changeTitArea">
                        <h4 class="js-titEn">GOOD</h4>
                        <h4 class="js-titJp">良い</h4>
                        <h4 class="js-titKr">좋은</h4>
                    </div>
                    <div class="pagMainRightBk-list--starArea">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                    </div>
                </div>
            </a>
            <a href="reason.php" class="pagMainRightBk-list">
                <h2 class="pagMainRightBk-list--chtit">普通</h2>
                <div class="pagMainRightBk-list--titStarArea">
                    <div class="pagMainRightBk-list--changeTitArea">
                        <h4 class="js-titEn">AVERAGE</h4>
                        <h4 class="js-titJp">平均</h4>
                        <h4 class="js-titKr">평균</h4>
                    </div>
                    <div class="pagMainRightBk-list--starArea">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                    </div>
                </div>
            </a>
            <a href="reason.php" class="pagMainRightBk-list">
                <h2 class="pagMainRightBk-list--chtit">差</h2>
                <div class="pagMainRightBk-list--titStarArea">
                    <div class="pagMainRightBk-list--changeTitArea">
                        <h4 class="js-titEn">POOR</h4>
                        <h4 class="js-titJp">貧しい</h4>
                        <h4 class="js-titKr">가난한</h4>
                    </div>
                    <div class="pagMainRightBk-list--starArea">
                        <img src="images/star.png" alt="star">
                        <img src="images/star.png" alt="star">
                    </div>
                </div>
            </a>
            <a href="reason.php" class="pagMainRightBk-list">
                <h2 class="pagMainRightBk-list--chtit">非常差</h2>
                <div class="pagMainRightBk-list--titStarArea">
                    <div class="pagMainRightBk-list--changeTitArea">
                        <h4 class="js-titEn">VERY POOR</h4>
                        <h4 class="js-titJp">非常に貧しい</h4>
                        <h4 class="js-titKr">매우 가난한</h4>
                    </div>
                    <div class="pagMainRightBk-list--starArea">
                        <img src="images/star.png" alt="star">
                    </div>
                </div>
            </a>
        </div>
    </div>
	

	
	
</body>
</html>
