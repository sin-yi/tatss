<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" /> -->
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" /> -->

<!-- 自動跳轉頁面 -->
<meta http-equiv="refresh" content="21;url= index.php ">

<?php require('head.php') ?>
<script language="javascript">
    
</script>



<body class="">
	<div class="pagReasonBk ptb-50 plr-50">
		<div class="pagReason-topBanner">
			<section class="modLogoTitArea">
				<img src="images/logo.png" alt="台中機場LOGO" class="modLogoTitArea-logo">
				<h5 class="modLogoTitArea-enTit modLogoTitArea-enTit--long">
					Taichung International<br />
					Airport  toilet satisfaction<br />
					survey system<br />
				</h5>
                <div class="modLogoTitArea-pageTitArea plr-50">
					<h2 class="modLogoTitArea-pageTitArea--chTit pt-30">選擇不滿意項目</h2>
					<div class="modLogoTitArea-pageTitArea--changeTit pt-12">
						<h5 class="js-titEn">Select unsatisfactory items</h5>
						<h5 class="js-titJp">物足りないものを選ぶ</h5>
						<h5 class="js-titKr">불만족스러운 항목 선택</h5>
					</div>
				</div>
			</section>
		</div>
        <div class="pagReason-mainArea">
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-01.svg" alt="無衛生紙" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">無衛生紙</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Run out of toilet paper</h5>
						<h5 class="js-titJp">トイレットペーパーが足りない</h5>
						<h5 class="js-titKr">화장지가 부족하다</h5>
					</div>
				</div>
			</a>
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-05.svg" alt="無洗手乳" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">無洗手乳</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Run out of hand wash gel</h5>
						<h5 class="js-titJp">手洗いジェルが不足しています</h5>
						<h5 class="js-titKr">핸드워시 젤 부족</h5>
					</div>
				</div>
			</a>
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-02.svg" alt="有異味" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">有異味</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Bad smell</h5>
						<h5 class="js-titJp">悪臭</h5>
						<h5 class="js-titKr">나쁜 냄새</h5>
					</div>
				</div>
			</a>
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-06.svg" alt="地板濕滑" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">地板濕滑</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Wet floor</h5>
						<h5 class="js-titJp">ぬれた床</h5>
						<h5 class="js-titKr">젖은 바닥</h5>
					</div>
				</div>
			</a>
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-03.svg" alt="垃圾未清" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">垃圾未清</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Full of trash</h5>
						<h5 class="js-titJp">ゴミだらけ</h5>
						<h5 class="js-titKr">쓰레기가 가득</h5>
					</div>
				</div>
			</a>
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-07.svg" alt="水槽髒汙" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">水槽髒汙</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Dirty sink</h5>
						<h5 class="js-titJp">汚れた流し</h5>
						<h5 class="js-titKr">더러운 싱크대</h5>
					</div>
				</div>
			</a>
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-04.svg" alt="馬桶髒污" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">馬桶髒污</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Dirty of toilet seat</h5>
						<h5 class="js-titJp">便座が汚れている</h5>
						<h5 class="js-titKr">변기의 더러운</h5>
					</div>
				</div>
			</a>
			<a href="result-bad.php" class="pagReason-mainArea--list">
				<img src="images/icon-08.svg" alt="設備故障" class="pagReason-mainArea--listIcon">
				<div class="pagReason-mainArea--listTextArea">
					<h3 class="pagReason-mainArea--chTit pb-5">設備故障</h3>
					<div class="pagReason-mainArea--cahngeTit">
						<h5 class="js-titEn">Repair equipment</h5>
						<h5 class="js-titJp">修理設備</h5>
						<h5 class="js-titKr">수리 장비</h5>
					</div>
				</div>
			</a>
        </div>
	</div>
	

	
	
</body>
</html>
