<!DOCTYPE html>	
<head>
<title></title>

<!-- 社群連結fb/line -->
<!-- 這裡要套 -->
<!-- <meta property="og:url"  content="" />
<meta property="og:type" content="website" />
<meta property="og:site_name" content="" />
<meta property="og:title" content="" />
<meta property="og:description" content="" />
<meta property="og:locale" content="zh_TW" />
<meta property="og:type" content="website" /> -->
<!-- 抓banner圖 -->
<!-- 如果你分享文章的縮圖要是寬版的大圖的話，那你的圖片至少要大於 600 x 315 px
最大圖片大小不能超過 5MB,圖片的寬高最大不能超過 1500 x 1500 px-->
<!-- <meta property="og:image" content="images/ogimages.png" />
<meta property="og:image:type" content="image/png" /> 
<meta property="og:image:width" content="1500" />
<meta property="og:image:height" content="1500" /> -->

<!-- 自動跳轉頁面 -->
<meta http-equiv="refresh" content="5;url= index.php ">

<?php require('head.php') ?>
<script>

    // setTimeout(() => {
        
    // }, 7000);
</script>





<body class="">
	<div class="pagResultBk ptb-50 plr-50">
		<div class="pagResult-topBanner">
			<section class="modLogoTitArea">
				<img src="images/logo.png" alt="台中機場LOGO" class="modLogoTitArea-logo">
				<h5 class="modLogoTitArea-enTit">
					Taichung International
					Airport  toilet satisfaction<br />
					survey system<br />
				</h5>
                <div class="modLogoTitArea-decArea"></div>
			</section>
		</div>
        <div class="pagResult-mainArea pt-55 plr-60">
			<img src="images/decTriangle-02.svg" alt="" class="pagResult-mainArea--dec">
            <h3 class="pb-40">Thank you for your feedback , we strive to service !</h3>
            <h1 class="pb-50">
				感謝您的寶貴意見 <br />
				我們會盡力改善！
            </h1>
            <h4 class="pb-12">貴重なコメントありがとうございます , 改善に全力を尽くします！</h4>
            <h4 class="">소중한 의견 감사합니다 , 우리는 개선하기 위해 최선을 다할 것입니다 !</h4>
        </div>
	</div>
	

	
	
</body>
</html>
